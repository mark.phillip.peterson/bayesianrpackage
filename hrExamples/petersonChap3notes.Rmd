---
title: "Notes while Reading chapter 3"
author: "Mark Peterson"
date: "06/26/2014"
output: 
  html_document:
    toc: true
    theme: cerulean

---


```{r, echo=FALSE} 
setwd("~/Documents/Juniata/teaching/learningBayes/myScripts")
```


The coin example seems trivial,
so let me try it with dice.
Note that the long run mean should be 3.5.

```{r}
set.seed(16652)
nRolls <- 10000
dieRolls <- sample(1:6,nRolls,replace = TRUE)

runningMean <- cumsum(dieRolls)/ cumsum(rep(1,nRolls))
plot(1:nRolls,runningMean,
     main="Running mean of die rolls",
     xlab = "Roll index")
abline(h=3.5,col="red",lwd=3)
```





