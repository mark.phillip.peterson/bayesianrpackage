Notes on homework
=================

These homework examples are simply intended to show how
the R package works, and I make no warranty about their accuracy.
Check out the solutions manual at
http://doingbayesiandataanalysis.blogspot.com/2012/06/solutions-to-exercises-now-available-to.html

In addition, early chapters of the homework used earlier versions of
the R package, and so may not currently work.
Once the package is more stable,
I will go back and make sure that they all work as intended.

Please, feel free to add your own solutions here as well.
It is especially great to compare approaches and priors,
and see how that influences the results.
