A project to convert Kruschke's Code
====================================

This repo is intended to convert the code provided with Kruschke's book
(Doing Bayesian Analysis) into an R package,
or at least more function based R code.
As part of our (Juniata College) summer reading group looking over the book,
we felt that cleaning the code could enhance student accessibility,
and reduce frustration among both the computer illiterate and the
professional programmer.


The book really is fantastic -- please buy it and use it.
The code here is intended to enhance, not replace,
what is so fantastically done in the book.


If you are completely new to R,
I suggest working through some sort of R tutorial before beginning.
There are lots of good ones, but I wrote a very, *very*, entry-level
tutorial that may be of use to you.
You can download it from:
https://bitbucket.org/petersmp/publishedtutorials/downloads
Download the *learningR_2014July21.pdf* (or a more current date)
and the *data.zip* folder, which contains the referenced data sets.


If you would like to help developing this R package,
please contribute as you are able.
For more information on using git or R packages, see the links
at the bottom of this README.


I ([Mark Peterson](mailto:mark.phillip.peterson@gmail.com) )
will compile the package as things get added,
and place an updated file in the downloads area.


To install this package
-----------------------

Once complete, this package will be submitted to cran.
For now, follow these directions to install.

Download the current tarball, but do not unpack it.
(To access Downloads, click on the small cloud icon to the left.)
In R, set your working directory and then run `install.packages()`
as below:

```
## NOTE: The "X" corresponds to the current version number
setwd("/path/to/where/you/saved/")
install.packages("kruschkeBayes_1.0X.tar.gz",repos=NULL,type="source")
```

Once installed, you can view which functions have currently been ported over using:

```
library(kruschkeBayes)
lsf.str("package:kruschkeBayes")
```


Version notes
-------------

This package is still **very unstable**.
In order to accomplish the goals of standardizing outputs,
several functions are likely to be change substantially *after*
they have been added to the package.
So, if you start working with this package,
it is advisable to stick with a version within a given script,
unless you need a bug fix or other update from a newer version.

Once the functions stabalize, I will change this note to include
a reference to a stable version and a log of changes from that version.

For more extensive examples of how the package works for the homework,
see the **hrExamples** directory.
Once the package stabalizes, these may be removed to avoid issues with
graded homework assignments.

**Version 1.10** Added a function for the home grown Metropolis of
two Bernouli processes and the jags version.
This generally works for the homework.
The only hiccup is that there is not an easy way to omit the data,
for generating the prior. My (temporary) solution was to skip that part.
I am not inclined to modify the the functions, just to allow that.
You may need to work on it from the original script if you really want to do it.

**Version 1.09** I am just starting to add the information from Chapter 8.
I Have added the function BernTwoGrid, but no other functions yet.
May be several days before the next round gets added.

**Version 1.08** This version *changes the output* of a few functions, so be aware. 
Primary change is that all of the bayes methods now return invisibly,
avoiding the problem of the massive prints to the screen when not saving outputs.
In addition *BernMetropolisTemplate* now also returns as a list,
and includes the thetaSample from the random walk to allow easier access for
some trouble shooting and other operations.

**Version 1.07** Add options to modify the starting position for the random walk
and the sd for the step sizes in the BernMetropolisTemplate function.
This version is sufficient to get through the chapter 7 homework,
though you will need to access some of the code
(either from here, or from Kruschke's original scripts)
in order to assess some of the intermediate steps,
and to complete Exercise 7.5.
See my examples in the Dropbox if you want to see how the code here
is used, as opposed to the originals.

**Version 1.06** Removes default ylim settings from plots of the BernGrid
and BernBeta functions to allow more user control and avoid artificially
squashed plots.

**Version 1.05** Adds the first of the JAGS calls in BernBetaJagsFull().
This version is likely sufficient for the chapter 7 homework,
but has not been tested on it yet.
We are now nearing the point where the benefit of functions may begin
to wane, and more scripts, which show the intermediate steps,
may be warranted.

**Version 1.04** adds the first MCMC tools, and adds further flexibility
to generated plots. It will work for the homework in chapter 6,
but none of the changes are necessary. You will likely need this version
for chapter 7, but it is not yet complete for chapter 7.

**Version 1.03** simply fixes my inability to spell,
and is safe to use for chapter 6 homework.
I recomend using it instead of 1.02 to avoid future spelling conflicts,
including for homework in chapter 6.

**Version 1.02 is complete for all features need through chapter 6**,
and works on the homework problems.
The outputs of some functions differ substantially from Kruschke's code
(in ways that I hope are improvements),
so see the help or my homework exampmles (posted on Dropbox)
for guidance on using some of the new information.





Goals of these changes
----------------------

- Add actual help files
	- One of the best features of R packages is that help files are required,
	  making the exact arguments and syntax of the function much easier to find.
- Standardize argument names
	- Several functions use similar input options, but different names
	  (e.g. credMass and credib).
	  Stanardizing these will make the package easier to use.
- Standardizing and expanding output values
	- Many functions calculate very useful information,
	  but in their current form only print it on the plots,
	  making it difficult to access.
	- By instead outputting slightly more information from the functions,
	  users will be better able to make use of the functions.
- Cleaning up plots
	- The default plots from these functions are messy,
	  especially on smaller plotting devices.
	- Taking the additional text off of the screen
	  (and returning it from the function instead)
	  will make the plots more versatile.
	- The current plotting approach over-rides user set parameters,
	  and does not reset them on exit.
	- Fixing this leaves user settings in place and reduces frustration.
	  This is accomplised through the use of

```
		## Save the current parameters
		opar <- par(no.readonly = TRUE)
		## Reset to them when the function exits
		on.exit(par(opar))
```



The contents of the repository are:
----------------------------------

- *trackingCodeUpdates.tsv*
	- A tab-separated file to track progress on the conversion
	- When you copy a file to the **kruschkeBayes/R/** or **modifiedScripts/**
		please mark it in this file so we can track progress
	- The file has two columns
		- The name of the file in **orignalScripts**
		- The status of the file
			- Blank if nothing has been done yet
			- "pacakge" if it was moved into the package
			- "modifiedScripts" if it was moved to the **modifiedScripts** directory
			- "notMoving" if the file should not be moved

- **orignalScripts**
	- The original script files provided by Dr. Kruschke at
		<http://www.indiana.edu/~kruschke/DoingBayesianDataAnalysis/Programs/ProgramsDoingBayesianDataAnalysis.zip>
	- The Rdata files provided with those scripts
	- The goal is to slowly port these files over to the R package
	- When you move a package, please mark it in the file *trackingCodeUpdates.tsv* so we can track progress

- **kruschkeBayes**
	- A directory to hold our R package in, please copy functions here when you work on them, 
		rather than modifying within **originalScripts**.
	- Add a comment to the top of the **originalScripts** files when you copy them to let others know.
	- **R**
		- Directory to hold all of the R code -- each function should be it's own script file
	- **man**
		- The help files for each function.
		- *EVERY* script file needs a help file
		- You can generate a template help file by running `prompt(functionName)`
			for each function, which will create a template file that you can move to **man** and edit
		- The man pages are essentially flavored LaTeX, so that may (or may not) help you understand them
		- They are probably easiest to edit within RStudio

- **modifiedScripts**
	- Some of the original scripts may not be suitable to turn into functions
	- For those, please copy them here and edit as needed.

- **hrExamples**
	- Examples of how the new package works for the homework problems.
	- Check out the included *README.md* more more information.



Information on using git
------------------------

Here is a link to a great git tutorial. <http://gitimmersion.com/lab_01.html>
I got through it in about an hour, but I know git and the commandline.
I expect it to take a novice with a small commandline background about 2-3 hours.

A few notes (READ before you begin; these were written for a class I taught):

- The tutorial uses two flags
	- *execute* which tells you to run the commands on the terminal and
	- *file* which tells you to save things in the file it mentions (e.g. in nano or gedit).

- When running the config steps, you can change your editor to nano (or another option) using:

```
git config --global core.editor nano
```

- You will be writing a series of programs in a language named Ruby.
	- Don't worry much about the syntax, it is just an example and we won't be using Ruby for this repo.
	- You can run the examples with "ruby FILENAME ARGUMENTS" where 
		- ruby tells the system to interpret the script as ruby,
		- FILENAME tells it where to look for the script, and
		- ARGUMENTS is a space separated list of arguments (which aren't always necessary).
	- Don't worry if you don't understand the code in the files: feel free to just copy and paste it.

- When you run rake, you will need to install the program, but ubuntu should tell you how to go about doing that.

- In lab 11, I recommend skipping the shell aliases for now- they can cause some issues, and won't be necessary for the amount you use git.

- STOP at lab 49 (unless you want to continue). We will be using bitbucket as our central repository (a word you will understand by lab 49),
	 so you won't need to do any of the local hosting things. Each of the labs are very short.

- After you get a feel for git, and additional tutorial
	<http://marklodato.github.io/visual-git-guide/index-en.html>
	may help you to visualize what git is doing at each step. 
	It is a nice addition, but I think is harder to follow than the walk through.

- For Windows users
	- Upgrade to a modern OS. [Ubuntu](http://www.ubuntu.com/download/desktop)
		works great out of the box, and can be installed alongside Windows
		(Unless Microsoft sufficiently breaks their hardware).
	- No? Ok, fine, there are options to use git in Windows, I just don't know them well
	- Git is designed to be used on plain text files, and Windows is not.
	- Here are a couple links that I found, which may (or may not) be useful
		- <http://git-scm.com/downloads> to download git
		- <http://net.tutsplus.com/tutorials/tools-and-tips/git-on-windows-for-newbs/>
		- <http://guides.beanstalkapp.com/version-control/git-on-windows.html>


Information on making R packages
--------------------------------

The best resource on making R packages is
<http://cran.r-project.org/doc/manuals/r-release/R-exts.html>
