BernBeta = function( priorShape ,
                     theData ,
                     nameSuccess = NULL,
                     credMass=0.95,
                     plot = TRUE,
                     ...) {
# Bayesian updating for Bernoulli likelihood and beta prior.
# Input arguments:
#   priorShape
#     vector of parameter values for the prior beta distribution.
#   dataVec
#     vector of 1's and 0's.
#   credMass
#     the probability mass of the HDI.
# Output:
#   postShape
#     vector of parameter values for the posterior beta distribution.
# Graphics:
#   Creates a three-panel graph of prior, likelihood, and posterior
#   with highest posterior density interval.
# Example of use:
# > postShape = BernBeta( priorShape=c(1,1) , dataVec=c(1,0,0,1,1) )
# You will need to "source" this function before using it, so R knows
# that the function exists and how it is defined.

  
# Check for errors in input arguments:
if ( length(priorShape) != 2 ) {
   stop("priorShape must have two components.") }
if ( any( priorShape <= 0 ) ) {
   stop("priorShape components must be positive.") }

if ( credMass <= 0 | credMass >= 1.0 ) {
   stop("credMass must be between 0 and 1.") }
if(!is.logical(plot)){
   stop("plot must be TRUE or FALSE") }
# Rename the prior shape parameters, for convenience:
a = priorShape[1]
b = priorShape[2]

## Convert Data from various forms
if(class(theData) == "numeric"){
  if ( any( theData != 1 & theData != 0 ) ) {
    stop("when numeric theData must be a vector of 1s and 0s.") }
} else {
  theData <- convertDataToBernoulli(theData,nameSuccess)
}


# Create summary values of the data:
z = sum( theData == 1 ) # number of 1's in dataVec
N = length( theData )   # number of flips in dataVec
# Compute the posterior shape parameters:
postShape = c( a+z , b+N-z )
# Compute the evidence, p(D):
pData = beta( z+a , N-z+b ) / beta( a , b )
# Determine the limits of the highest density interval.
# This uses a home-grown function called HDIofICDF.
# source( "HDIofICDF.R" )
hpdLim = HDIofICDF( qbeta , shape1=postShape[1] , shape2=postShape[2] , credMass=credMass )

# Now plot everything:
# Construct grid of theta values, used for graphing.
binwidth = 0.005 # Arbitrary small value for comb on Theta.
Theta = seq( from = binwidth/2 , to = 1-(binwidth/2) , by = binwidth )
# Compute the prior at each value of theta.
pTheta = dbeta( Theta , a , b )
# Compute the likelihood of the data at each value of theta.
pDataGivenTheta = Theta^z * (1-Theta)^(N-z)
# Compute the posterior at each value of theta.
pThetaGivenData = dbeta( Theta , a+z , b+N-z )

if(plot){
  
  ## Reset parameter on exit
  opar <- par(no.readonly = TRUE)  
  on.exit(par(opar)) 
  
  ## Set the layout
  par( mar=c(3,3,1,0) , mgp=c(2,1,0) , mai=c(0.5,0.5,0.3,0.1) ) # margin specs
  layout( matrix( c( 1,2,3 ) ,nrow=3 ,ncol=1 ,byrow=FALSE ) ) # 3x1 panels
  
  
  ## Set default Params
  defaultParams <- list(
    type = "l" ,
    xlim = c(0,1) ,
    ## Commenting out to just let R pick the limits
    # ylim=c(0,1.1*max(pTheta)) , 
    xlab=as.expression(bquote(theta) ),
    ylab=as.expression(bquote(p(theta)) ) ,
    main="Prior"   
  )
  
  ## Read in the use supplied additional args
  inDots <- list(...)
  
  ## Set the value of inDots for anything the use didn't supply
  for(thisParam in names(defaultParams)){
    if(is.null(inDots[[thisParam]])){
      inDots[[thisParam]] <- defaultParams[[thisParam]]
    }
  }
  
  ## Set the value of the data for plotting
  inDots$x <- Theta
  inDots$y <- pTheta
  
  ## plot the prior with parameters set above
  do.call("plot",inDots)    
  
  
  ## Set the value of the data for plotting the likelihood: p(Data|Theta)
  inDots$x <- Theta
  inDots$y <- pDataGivenTheta
  ## Commenting out to just let R pick the limits
  ## Option in BernGrid allows only over-riding if not supplied.
  #inDots$ylim <- c(0,1.1*max(pDataGivenTheta))
  inDots$main <- "Likelihood"
  
  ## Run the plot with parameters set above
  do.call("plot",inDots)
  
  ## Set the value of the data for plotting the the posterior
  inDots$x <- Theta
  inDots$y <- pThetaGivenData
  inDots$main <- "Posterior"
  inDots$ylab <- as.expression(bquote( "p(" * theta * "|D)" ))
  ## Commenting out to just let R pick the limits
  ## Option in BernGrid allows only over-riding if not supplied.
  # inDots$ylim <- c(0,1.1*max(pThetaGivenData))
  
  ## Run the plot with parameters set above
  do.call("plot",inDots)
  
  
}

HDItemp <- as.data.frame(matrix(hpdLim,nrow=1,dimnames = list(c(""),c("min","max"))))

out <- list()
out$posteriorShape <- postShape
out$meanPosterior <- postShape[1] / sum(postShape)
out$HDIranges <- HDItemp
out$pData <- pData
invisible( out )

} # end of function
