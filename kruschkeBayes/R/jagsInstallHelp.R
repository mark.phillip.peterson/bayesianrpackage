#### Return install directions
jagsInstallHelp <- function(){
  
  message(
    "To install JAGS, the non-R program:\n",
    "go to: http://mcmc-jags.sourceforge.net/ and follow the directions.\n\n",
    
    "To install rjags, the R package that calls JAGS:\n",
    "After installing jags, use install.packages('rjags') to install rjags.")

}