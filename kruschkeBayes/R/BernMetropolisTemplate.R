BernMetropolisTemplate <- function(
  prior = "uniform",
  likelihood = "standard",
  targetRelProb = "standard",
  theData = factor(rep(c("H","T"),times=c(8,4))),
  nameSuccess = NULL,
  trajLength = 10000,
  burnIn = 0.1 * trajLength ,
  startingValue = 0.5,
  jumpSD = 0.1,
  plot = TRUE,
  ...
  ){

  # Use this program as a template for experimenting with the Metropolis
  # algorithm applied to a single parameter called theta, defined on the 
  # interval [0,1].
  
  if(class(theData) == "numeric"){
    if ( any( theData != 1 & theData != 0 ) ) {
      stop("when numeric theData must be a vector of 1s and 0s.") }
  } else {
    theData <- convertDataToBernoulli(theData,nameSuccess)
  }
  # Define the Bernoulli likelihood function, p(D|theta).
  # The argument theta could be a vector, not just a scalar.
  if(class(likelihood) == "character"){
    if(likelihood == "standard"){
      likelihood = function( theta , data ) {
        z = sum( data == 1 )
        N = length( data )
        pDataGivenTheta = theta^z * (1-theta)^(N-z)
        # The theta values passed into this function are generated at random,
        # and therefore might be inadvertently greater than 1 or less than 0.
        # The likelihood for theta > 1 or for theta < 0 is zero:
        pDataGivenTheta[ theta > 1 | theta < 0 ] = 0
        return( pDataGivenTheta )
      }
      
    }
    
  }
  
  # Define the prior density function. For purposes of computing p(D),
  # at the end of this program, we want this prior to be a proper density.
  # The argument theta could be a vector, not just a scalar.
  
  if(class(prior) == "character"){
    if(prior =="uniform"){
      prior = function( theta ) {
        prior = rep( 1 , length(theta) ) # uniform density over [0,1]
        # For kicks, here's a bimodal prior. To try it, uncomment the next line.
        #prior = dbeta( pmin(2*theta,2*(1-theta)) ,2,2 )
        # The theta values passed into this function are generated at random,
        # and therefore might be inadvertently greater than 1 or less than 0.
        # The prior for theta > 1 or for theta < 0 is zero:
        prior[ theta > 1 | theta < 0 ] = 0
        return( prior )
      }    
    } else if(prior == "bimodal"){
      prior = function( theta ) {
        prior = dbeta( pmin(2*theta,2*(1-theta)) ,2,2 )
        prior[ theta > 1 | theta < 0 ] = 0
        return( prior )
      }
    }
    
  }
  
  # Define the relative probability of the target distribution, 
  # as a function of vector theta. For our application, this
  # target distribution is the unnormalized posterior distribution.
  if(class(targetRelProb) == "character"){
    if(targetRelProb == "standard"){
      targetRelProb = function( theta , data ) {
        targetRelProb =  likelihood( theta , data ) * prior( theta )
        return( targetRelProb )
      }    
    }    
  }

  # Initialize the vector that will store the results:
  trajectory = rep( 0 , trajLength )
  # Specify where to start the trajectory:
  trajectory[1] = startingValue
  # Initialize accepted, rejected counters, just to monitor performance:
  nAccepted = 0
  nRejected = 0
  
  # Now generate the random walk. The 't' index is time or trial in the walk.
  for ( t in 1:(trajLength-1) ) {
    currentPosition = trajectory[t]
    # Use the proposal distribution to generate a proposed jump.
    # The shape and variance of the proposal distribution can be changed
    # to whatever you think is appropriate for the target distribution.
    proposedJump = rnorm( 1 , mean = 0 , sd = jumpSD )
    # Compute the probability of accepting the proposed jump.
    probAccept = min( 1,
                      targetRelProb( currentPosition + proposedJump , theData )
                      / targetRelProb( currentPosition , theData ) )
    # Generate a random uniform value from the interval [0,1] to
    # decide whether or not to accept the proposed jump.
    if ( runif(1) < probAccept ) {
      # accept the proposed jump
      trajectory[ t+1 ] = currentPosition + proposedJump
      # increment the accepted counter, just to monitor performance
      if ( t > burnIn ) { nAccepted = nAccepted + 1 }
    } else {
      # reject the proposed jump, stay at current position
      trajectory[ t+1 ] = currentPosition
      # increment the rejected counter, just to monitor performance
      if ( t > burnIn ) { nRejected = nRejected + 1 }
    }
  }
  
  # Extract the post-burnIn portion of the trajectory.
  acceptedTraj = trajectory[ (burnIn+1) : length(trajectory) ]
  
  # End of Metropolis algorithm.
  
  #-----------------------------------------------------------------------
  # Display the posterior.
  mcmcInfo = plotPost( acceptedTraj , xlim=c(0,1) ,
                       xlab=as.expression(bquote(theta)) ,
                       plot = plot, ...)
  
  # Display rejected/accepted ratio in the plot.
  # Get the highest point and mean of the plot for subsequent text positioning:
  mcmcInfo$propAccepted <- nAccepted/length(acceptedTraj)
  
  #------------------------------------------------------------------------
  # Evidence for model, p(D).
  
  # Compute a,b parameters for beta distribution that has the same mean
  # and stdev as the sample from the posterior. This is a useful choice
  # when the likelihood function is Bernoulli.
  densMax = max( density( acceptedTraj )$y )
  meanTraj = mean( acceptedTraj )
  sdTraj = sd( acceptedTraj )
  
  a =   meanTraj   * ( (meanTraj*(1-meanTraj)/sdTraj^2) - 1 )
  b = (1-meanTraj) * ( (meanTraj*(1-meanTraj)/sdTraj^2) - 1 )
  
  # For every theta value in the posterior sample, compute 
  # dbeta(theta,a,b) / likelihood(theta)*prior(theta)
  # This computation assumes that likelihood and prior are proper densities,
  # i.e., not just relative probabilities. This computation also assumes that
  # the likelihood and prior functions were defined to accept a vector argument,
  # not just a single-component scalar argument.
  wtdEvid = dbeta( acceptedTraj , a , b ) / (
    likelihood( acceptedTraj , theData ) * prior( acceptedTraj ) )
  mcmcInfo$pData = 1 / mean( wtdEvid )
  
  out <- list()
  out$thetaSample <- acceptedTraj
  out$mcmcInfo <- mcmcInfo
  invisible(out)
}



