BernTwoGrid = function( theta1 = NULL ,
                        theta2 = NULL ,
                        prior = "Beta", # Can be named or a matrix passed in
                        priorOptions = NULL , # options passed into named prior, must be null or list or match order
                        nTheta = 501 , # n Thetas to create
                        theData1 = factor(rep(c("H","T"),c(5,2))),
                        theData2 = factor(rep(c("H","T"),c(2,5))),
                        nameSuccess = NULL,
                        likeAtPoint = NULL , # Function for the likelihood
                        perspOptions = list() , # perspective specific options
                        contourOptions = list() ,
                        credMass = .95 ,
                        plot = TRUE,
                        nToPlot=31,
                        ...) {
  
  if(class(theData1) == "numeric"){
    if ( any( theData1 != 1 & theData1 != 0 ) ) {
      stop("when numeric theData1 must be a vector of 1s and 0s.") }
  } else {
    theData1 <- convertDataToBernoulli(theData1,nameSuccess)
  }
  
  if(class(theData2) == "numeric"){
    if ( any( theData2 != 1 & theData2 != 0 ) ) {
      stop("when numeric theData2 must be a vector of 1s and 0s.") }
  } else {
    theData2 <- convertDataToBernoulli(theData2,nameSuccess)
  }
  
  # Get number from the data for use
  z1 = sum(theData1)
  N1 = length(theData1)
  z2 = sum(theData2)
  N2 = length(theData2)
  
  # Specify the grid on theta1,theta2 parameter space.
  if(is.null(theta1)){
    theta1 = seq( from=((1/nTheta)/2) ,to=(1-((1/nTheta)/2)) ,by=(1/nTheta) )
  }
  if(is.null(theta2)){
    theta2 = seq( from=((1/nTheta)/2) ,to=(1-((1/nTheta)/2)) ,by=(1/nTheta) )
  }
  
  # nInt = 501 # arbitrary number of intervals for grid on theta.
  # theta1 = seq( from=((1/nInt)/2) ,to=(1-((1/nInt)/2)) ,by=(1/nInt) )
  # theta2 = theta1 
  
  # Specify the prior probability _masses_ on the grid.
  if(class(prior) == "character"){
    namedPriors <- c("Beta","Ripples","Null","Alt")
    if(!prior %in% namedPriors){
      stop("prior must either be a numeric matrix or one of: ", paste(namedPriors,collapse = ", "))
    } else {
      # Set priors based on the names
      if ( prior == "Beta" ) {
        if(is.null(priorOptions)){
          priorOptions <- list(a1 = 3, b1 = 3, a2 = 3, b2 = 3)
        } else if(class(priorOptions) == "numeric"){
          priorOptions <- as.list(priorOptions)
          names(priorOptions) <- c("a1","b1","a2","b2")
        }
      
        prior1 = dbeta( theta1 , priorOptions$a1 , priorOptions$b1 )
        prior2 = dbeta( theta2 , priorOptions$a2 , priorOptions$b2 )
        prior = outer( prior1 , prior2 ) # density
        prior = prior / sum( prior ) # convert to normalized mass
      } else if ( prior == "Ripples" ) {
        # Set ripple prior
        rippleAtPoint = function( theta1 , theta2) {
          if(is.null(priorOptions)){
            priorOptions <- list(m1 = 0, m2 = 1, k = 0.75*pi)
          } else if(class(priorOptions) == "numeric"){
            priorOptions <- as.list(priorOptions)
            names(priorOptions) <- c("m1","m2","k")
          }
          sin( (priorOptions$k*(theta1-priorOptions$m1))^2 +
                 (priorOptions$k*(theta2-priorOptions$m2))^2 )^2 
        }
        prior = outer( theta1 , theta2 , rippleAtPoint )
        prior = prior / sum( prior ) # convert to normalized mass
      } else if ( prior == "Null" ) {
        # 1's at theta1=theta2, 0's everywhere else:
        prior = diag( 1 , nrow=length(theta1) , ncol=length(theta1) )
        prior = prior / sum( prior ) # convert to normalized mass
      } else if ( prior == "Alt" ) {
        # Uniform:
        prior = matrix( 1 , nrow=length(theta1) , ncol=length(theta2) )
        prior = prior / sum( prior ) # convert to normalized mass
      }
      
    }
  }
  
  if(is.null(row.names(prior) )){
    row.names(prior) <- theta1
  }
  if(is.null(colnames(prior) )){
    colnames(prior) <- theta2
  }
  

  # Specify likelihood
  if(is.null(likeAtPoint)){
    likeAtPoint = function( t1 , t2 ) {
      p = t1^z1 * (1-t1)^(N1-z1) * t2^z2 * (1-t2)^(N2-z2)
      return( p )
    }
  }
  likelihood = outer( theta1 , theta2 , likeAtPoint )
  
  # Compute posterior from point-by-point multiplication and normalizing:
  pData = sum( prior * likelihood )
  posterior = ( prior * likelihood ) / pData
  
  # Store HDI for plotting and return
  HDIinfo = HDIofGrid( posterior )
  # ---------------------------------------------------------------------------
  
  # Specify aspects of perspective and contour plots.
  # IF plotting
  if(plot){
    
    ## Set default Params
    defaultParams <- list(
      xlab="theta1" ,
      ylab="theta2" ,
      xlim=c(0,1) ,
      ylim=c(0,1)
      
    )
    
    ## Read in the use supplied additional args
    inDots <- list(...)
    
    ## Set the value of inDots for anything the use didn't supply
    for(thisParam in names(defaultParams)){
      if(is.null(inDots[[thisParam]])){
        inDots[[thisParam]] <- defaultParams[[thisParam]]
      }
    }
    
    # Set values for perspective separately
    defaultsPersp <- list(
      # Persp specific params
      theta = (-25),
      phi = 25,
      d = 5.0,
      shade = 0.05,
      cex = 0.7,
      lwd=0.1 ,
      zlab = "p(t1,t2)"
    )
    
    ## Set the value of perspOptions for anything the use didn't supply
    for(thisParam in names(defaultsPersp)){
      if(is.null(perspOptions[[thisParam]])){
        perspOptions[[thisParam]] <- defaultsPersp[[thisParam]]
      }
    }
    
    # Set values for contour separately
    defaultsContour <- list(
      nlevels = 9,
      drawlabels=FALSE
    )
    
    ## Set the value of perspOptions for anything the use didn't supply
    for(thisParam in names(defaultsContour)){
      if(is.null(contourOptions[[thisParam]])){
        contourOptions[[thisParam]] <- defaultsContour[[thisParam]]
      }
    }
        
    # Specify the indices to be used for plotting. The full arrays would be too
    # dense for perspective plots, so we plot only a thinned-out set of them.
    if(is.null(nToPlot)){
      inDots$x <- theta1[thindex1]
      inDots$y <- theta2[thindex2]
    } else {
      thindex1 = round(seq( 1, length( theta1 ) , length.out = nToPlot ))
      thindex2 = round(seq( 1, length( theta2 ) , length.out = nToPlot ))
      
      inDots$x <- theta1[thindex1]
      inDots$y <- theta2[thindex2]
    }
    
    ## Reset parameter on exit
    opar <- par(no.readonly = TRUE)  
    on.exit(par(opar)) 
    
    layout( matrix( c( 1,2,3,4,5,6 ) ,nrow=3 ,ncol=2 ,byrow=TRUE ) ) 
    par(mar=c(3,3,1,0))          # number of margin lines: bottom,left,top,right
    par(mgp=c(2,1,0))            # which margin lines to use for labels
    par(mai=c(0.4,0.4,0.2,0.05)) # margin size in inches: bottom,left,top,right
    par(pty="s")                 # makes contour plots in square axes.
    
    # prior
    perspOptions$main <- "Prior"
    inDots$z <- prior[thindex1,thindex2]
    do.call(persp,c(inDots,perspOptions))
    do.call(contour,c(inDots,contourOptions))
    
    # likelihood
    perspOptions$main <- "Likelihood"
    inDots$z <- likelihood[thindex1,thindex2]
    do.call(persp,c(inDots,perspOptions))
    do.call(contour,c(inDots,contourOptions))
    
    # posterior
    perspOptions$main <- "Posterior"
    inDots$z <- posterior[thindex1,thindex2]
    do.call(persp,c(inDots,perspOptions))
    do.call(contour,c(inDots,contourOptions))
    
    # Mark the highest posterior density region
    par(new=TRUE) # don't erase previous contour
    contourOptions$levels = signif(HDIinfo$height,3)
    contourOptions$lwd = 3
    do.call(contour,c(inDots,contourOptions))
    
  }

  meanTheta1GivenData = sum( theta1 * apply(posterior,1,sum) )
  meanTheta2GivenData = sum( theta1 * apply(posterior,2,sum) )
  
  # Get meaningful HDI info:
  inHDIx <- numeric()
  inHDIy <- numeric()
  for(j in thindex1){
    for(k in thindex2){
      if(posterior[j,k] > HDIinfo$height){
        inHDIx <- c(inHDIx,row.names(posterior)[j])
        inHDIy <- c(inHDIy,colnames(posterior)[k])
      }
    }
  }
  inHDI <- data.frame(theta1 = inHDIx, theta2 =inHDIy)
  
  out <- list()
  out$posterior <- posterior
  out$meanPosterior <- c(theta1 = meanTheta1GivenData, theta2 = meanTheta2GivenData) 
  out$inHDI <- inHDI 
  out$pData <- pData
  invisible( out )
  
}