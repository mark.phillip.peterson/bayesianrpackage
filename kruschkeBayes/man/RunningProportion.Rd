\name{RunningProportion}
\alias{RunningProportion}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Calculate and plot a running proportion
}
\description{
Generates data from a Bernouli procces and calcultes the running proportion.
}
\usage{
RunningProportion(N = 500, x = c("H", "T"), nameSuccess = "H", probSuccess = 0.5, theData = NULL, plot = TRUE, ...)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{N}{
  Numeric, how many values should be simulated
}
  \item{x}{
  Vector of length two giving the items to generate from, e.g. heads and tails.
}
  \item{nameSuccess}{
  Which value of \code{x} or from \code{theData} should be treated as a success,
  if they are not already numeric?
  If NULL, and \code{theData} is a factor, the first level will be treated as success.
}
  \item{probSuccess}{
  For generating the data, and plotting either,
  what is the underlying probability of getting a success?
  Numeric between 0 and 1.
}
  \item{theData}{
  A vector of data to be used.
  If this is set (i.e., not NULL), options for generating data are ignored.
  Can be numeric of 1's and 0's (for success and failure),
  a factor, with the first level (or the level named in \code{nameSuccess})
  representing success, or
  a character vector with the level named in \code{nameSuccess})
  representing success. 
}
  \item{plot}{
  Logical. Should the plots be generated?
}
  \item{\dots}{
  Additional arguments passed to plot.
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
Because these variables are likely to be long, this function returns invisibly.
Invisibly returns a list containing:
  \item{theData}{The data generated by the process, to allow further inspection}
  \item{runProp}{The running proportion of successes at each point in the process}
  \item{finalProp}{The final proportion of successes in the run.}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
Mark Peterson, borrowed heavily from John Kruschke
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
a <- RunningProportion()
a$finalProp
a


}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
