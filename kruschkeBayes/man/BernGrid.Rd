\name{BernGrid}
\alias{BernGrid}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Grid approximation of Bayesian posterior of Bernouli process
}
\description{
Calculates the Bayesian posterior of a Bernouli process over
a discrete grid of theta values
given a prior and some data.
}
\usage{
BernGrid(Theta,
         pTheta,
         theData,
         nameSuccess = NULL,
         credMass = 0.95,
         plot = TRUE,
         nToPlot = length(Theta),
         ...)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{Theta}{
  A numerical vector of potential theta values between 0 and 1.
}
  \item{pTheta}{
  A vector of the same length as \code{Theta} giving
  the prior probablitiy of each Theta value.
  Will be normalized to sum to 1 by the function.
}
  \item{theData}{
  A vector of data for updating the posterior.
  Can be numeric of 1's and 0's (for success and failure),
  a factor, with the first level (or the level named in \code{nameSuccess})
  representing success, or
  a character vector with the level named in \code{nameSuccess})
  representing success. 
}
  \item{nameSuccess}{
  The value of a \code{theData} to represent a success;
  if \code{NULL} and \code{theData} is a factor,
  the first level is assumed to be a success.
}
  \item{credMass}{
  The probability mass of the HDI,
  how wide do you want the plotted HDI to be.
}
  \item{plot}{
  Logical, should the prior, data, and posterior be plotted?
}
  \item{nToPlot}{
  How many of the points in \code{Theta} should be plotted?
  Defaults to including all of the values.
}
  \item{\dots}{
  Additional arguments passed to plot.
  e.g. \code{col = "skyblue"} to match Kruschke examples.
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
Invisibly returns a list with the following components:

\item{posterior}{A vector of the posterior probabilities,
                  with names corresponding to \code{Theta} }
\item{meanPosterior}{ The weighted mean of the posterior, i.e., the predicted Theta }
\item{HDIranges}{Ranges encompassing the HDI interval requested}
\item{pData}{The total probability of the data, useful for model comparison}

and plots the Prior, Likelihood, and Posterior, if \code{plot = TRUE}.
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
Mark Peterson, borrowed heavily from John Kruschke
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{

Theta <- c(.25,.5,.75)
post <- BernGrid( Theta ,c(.3,.4,.3),c("H","H","T"),"H")
post

## Nest to use the posterior
postB <- BernGrid( Theta, post$posterior, c("H","H","T"),"H")
postB

## More complicated prior
Theta <- seq(0,1,0.05)
post <- BernGrid( Theta, pmin(Theta, 1- Theta),factor(c("H","T","T","H","H","H")))

## Suppress the posterior values for cleaner display
post[ -1 ]


############################################
## To make Figure 6.4 from Kruschke's book: ##
############################################

# Specify theta values.
thetagrid = seq(0,1,length=1001)

# Specify probability mass at each theta value.
relprob = sin( 2*pi*thetagrid )^6
prior = relprob / sum(relprob) # probability mass at each theta

# Specify the data vector.
theData = theData = factor( rep( c("H","T"), c(2,1) ) )


posterior = BernGrid( Theta=thetagrid , pTheta=prior , theData= theData )
posterior[-1]

############################################
## To make Figure 6.5 from Kruschke's book: ##
############################################

## Set thetas and priors
pTheta = c( 50:1 , rep(1,50) , 1:50 , 50:1 , rep(1,50) , 1:50 )
pTheta = pTheta / sum( pTheta )
width = 1 / length(pTheta)
Theta = seq( from = width/2 , to = 1-width/2 , by = width )

## Set and run the left panel
theData = factor( rep( c("H","T"), c(3,1) ) )
posterior = BernGrid( Theta=Theta , pTheta=pTheta , theData= theData )
posterior[-1]

## Set and run the right panel
theDataB = factor( rep( c("H","T"), c(12,4) ) )
posteriorB = BernGrid( Theta=Theta , pTheta=posterior$posterior , theData=theDataB )

}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
