\name{HDIofMCMC}
\alias{HDIofMCMC}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Calculate HDI from a MCMC output
}
\description{
Determines the Highest Density Interval (HDI)
from a vector of parameter values from a Markov Chain Monte Carlo.
This is usually called from within another function
but may be useful for pulling out more explicit information.
}
\usage{
HDIofMCMC(sampleVec, credMass = 0.95)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{sampleVec}{
  A vector of the samples from the MCMC process.
}
  \item{credMass}{
The desired mass of the HDI region.
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
Returns a vector of length two giving the minimum and maximum of the HDI.
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
Mark Peterson, copied largely from John Kruschke
}
\note{
This function only allows a single HDI,
rather than the bimodal possible in \code{\link{HDIofGrid}}.
Because of the nature of the MCMC approach,
the HDI is calculated as the narrowest continuous region
encompassing the desired \code{credMass}
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
\code{\link{HDIofGrid}}, \code{\link{HDIofICDF}}
}
\examples{
## Simulate the output of a random walk:
## Note this is NOT how you would do it,
## but the values work for the plot
walkVals <- rnorm(1000, 0.6, 0.1)
HDIofMCMC( walkVals )

}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
