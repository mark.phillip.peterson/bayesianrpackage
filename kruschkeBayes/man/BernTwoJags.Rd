\name{BernTwoJags}
\alias{BernTwoJags}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Conduct a Bernouli test of two parameters in JAGS
}
\description{
Works with JAGS to run a Bayesian test on two Bernouli parameters
}
\usage{
BernTwoJags(prior = "Beta",
            priorOptions = NULL,
            modelSaveFile = ".tmpModel.txt",
            theData1 = factor(rep(c("H", "T"), c(5, 2))),
            theData2 = factor(rep(c("H", "T"), c(2, 5))),
            nameSuccess = NULL,
            parameters = c("theta1", "theta2"),
            adaptSteps = 500,
            burnInSteps = 1000,
            nChains = 3,
            numSavedSteps = 50000,
            thinSteps = 1,
            nIter = ceiling((numSavedSteps * thinSteps)/nChains),
            plot = TRUE,
            predictValues = FALSE,
            ...)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{prior}{
  Either "Beta" with options optionally set by \code{priorOptions}, or
  a character vector of length one specifying the model to be used, in JAGS format.
  If set to NULL, the progam will read a file from "modelSaveFile" and use that directly.
  Note: Addtional preset priors may be added in the future.
}
  \item{priorOptions}{
  Options to be passed into the prior functions if giving a named value,
  or NULL if no options are to be passed.
  See Details for more information.
}
  \item{modelSaveFile}{
  Character sting giving the file name where the model should be saved,
  or naming the file to be read, if \code{model = NULL}.
}
  \item{theData1}{
  A vector of data for updating the posterior for \code{theta1}.
  Can be numeric of 1's and 0's (for success and failure),
  a factor, with the first level (or the level named in \code{nameSuccess})
  representing success, or
  a character vector with the level named in \code{nameSuccess})
  representing success. 
}
  \item{theData2}{
  A vector of data for updating the posterior for \code{theta2}.
  Can be numeric of 1's and 0's (for success and failure),
  a factor, with the first level (or the level named in \code{nameSuccess})
  representing success, or
  a character vector with the level named in \code{nameSuccess})
  representing success. 
}
  \item{nameSuccess}{
  The value of a \code{theData} to represent a success;
  if \code{NULL} and \code{theData} is a factor,
  the first level is assumed to be a success.
}
  \item{parameters}{
  Character vector of the parameters, from \code{modelString},
  that should be saved.
}
  \item{adaptSteps}{
  Number of steps to "tune" the samplers
}
  \item{burnInSteps}{
  Number of steps to "burn in" the samplers
}
  \item{nChains}{
  Number of chains to run.
}
  \item{numSavedSteps}{
  Total number of steps in chains to save.
}
  \item{thinSteps}{
  Number of steps to "thin" (1=keep every step).
}
  \item{nIter}{
  Number of steps per chain
}
  \item{plot}{
  Logical, should a plot be drawn, or should the values just be returned?
}
  \item{predictValues}{
  Logical, should predicted values be generated.
  Note, this can be a long process.
}
  \item{\dots}{
  Additional parameters passed to \code{\link{plotPost}}.
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
Invisibly returns a list with
\item{thetaSample}{A vector of length \code{numSavedSteps}
    (as modified by \code{thinSteps})
    giving the generated values of theta from jags}
\item{mcmcInfo}{A data.frame with the summary information as provided by \code{\link{plotPost}},
    for the sampled theta values}
\item{modelInfo}{A list with the model specifications, including
    \code{model} with the specified model, and \code{data} with the data specified in
    \code{theData}}
\item{PredictedValues}{A matrix with predict outcomes (0 = failure, 1 = success)
    for the two parameters.}
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
Mark Peterson, adapted from John Kruschke.
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
BernTwoJags()
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
